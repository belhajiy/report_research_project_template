
from pathlib import Path
import math


from scipy.integrate import solve_ivp
from scipy.integrate import odeint
import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits import mplot3d
import numpy as np
import matplotlib.pyplot as plt

def solution2DDT(A,B,C,D,X0,Y0): #solution en fct du temps
    #periode de l'etude 
    t0= 0
    tf= 50
    #systeme d'equation diff couplé 
    def diffcouple(t,variable,A,B,C,D):
        X=variable[0]
        Y=variable[1]    
        dX_dt= A*X-B*X*Y    #evolution des proie
        dY_dt= -C*Y+D*X*Y   #evolution des predateur          
        return [dX_dt,dY_dt]
    #resolution du systeme 
    t=np.linspace(t0,tf,200)
    solution = solve_ivp(diffcouple, [t0, tf], [X0, Y0], method='RK45', args=(A,B,C,D), max_step=0.01)
    X=solution.y[0] #population de proie
    Y=solution.y[1] #population de predateur 
    t=solution.t    #vecteur temps 
    #tracer les resultat 
    plt.plot(t, X, label="population de proie")
    plt.plot(t, Y, label="population de predateur",color='red')
    plt.legend()
    plt.xlabel("Temps")
    plt.ylabel("Proie et prédateur")
    fig=plt.gcf()
    return fig
    



here = Path(__file__).absolute().parent
path_dir_save = here / "../fig/"
path_dir_save.mkdir(exist_ok=True)

#solution en fct du temps
sol1=solution2DDT(1.2,0.4,0.4,0.1,11,3)
sol1.savefig(path_dir_save / "1ProieAndPredateurVsTimenewparam.png")

plt.show()

sol2=solution2DDT(1.1,0.3,0.4,0.2,3,10)
sol2.savefig(path_dir_save / "2ProieAndPredateurVsTimenewparam.png")

plt.show()
